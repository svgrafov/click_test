<?php
declare(strict_types=1);

namespace Tests\unit\AppBundle\Service;

use AppBundle\Service\ReportValidator;
use \PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ReportValidatorTest extends TestCase
{
    /**
     * There's not much to test really
     */
    public function testValidate()
    {
        $symfonyValidator = $this->getMockForAbstractClass(ValidatorInterface::class);
        $symfonyValidator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());
        $service = new ReportValidator($symfonyValidator);
        $this->assertInstanceOf(ConstraintViolationList::class, $service->validate('test'));
    }
}