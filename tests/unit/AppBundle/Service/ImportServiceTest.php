<?php
declare(strict_types=1);

namespace Tests\unit\AppBundle\Service;

use AppBundle\Repository\ReportRepository;
use AppBundle\Service\ImportService;
use AppBundle\Service\ReportValidator;
use PHPUnit\Framework\TestCase;
use SplFileObject;
use Symfony\Component\Validator\ConstraintViolationList;

class ImportServiceTest extends TestCase
{
    public function testImport()
    {
        $object = new SplFileObject(__DIR__.'/../../../testdata/1.csv');
        $validator = $this->createMock(ReportValidator::class);
        $repository = $this->getMockBuilder(ReportRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['import'])
            ->getMock();

        $repository->expects($this->exactly(1))
            ->method('import')
            ->withConsecutive(
                [$this->equalTo([
                    ['date' => '2018-01-01','geo' => 'ZZ','zone' => '1111111', 'impressions' => '100','revenue' => '0.25'],
                    ['date' => '2018-01-03','geo' => 'RU','zone' => '1111111', 'impressions' => '200','revenue' => '0.5'],
                    ['date' => '2018-01-01','geo' => 'RU','zone' => '1111111', 'impressions' => '600','revenue' => '1.5']])]
            );
        $validator->expects($this->exactly(9))
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $service = new ImportService($repository, $validator);
        $this->assertTrue($service->import($object));
    }



}