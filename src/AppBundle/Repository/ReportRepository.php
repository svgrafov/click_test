<?php
declare(strict_types=1);

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;

class ReportRepository
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param array $reportData
     * @throws \Doctrine\DBAL\DBALException
     * формат [[<date>, <geo>, <zone>, <impressions>, <revenue>]]
     */
    public function import(array $reportData): void
    {
        $connection = $this->em->getConnection();

        $params = [];
        $placeholders = '';
        foreach ($reportData as $item) {
            $params = array_merge($params, array_values($item));
            $placeholders .= '(?,?,?,?,?),';
        }

        $placeholders = rtrim($placeholders, ',');
        $sql = "
            INSERT INTO report (`date`, geo, zone, impressions, revenue)
            VALUES $placeholders
            ON DUPLICATE KEY UPDATE
                impressions = impressions + VALUES(impressions),
                revenue     = revenue + VALUES(revenue);
        ";
        $stmt = $connection->prepare($sql);
        $stmt->execute($params);
    }

}