<?php
declare(strict_types=1);

namespace AppBundle\Entity;

use DateTimeImmutable;

/**
 * Report
 */
class Report
{
    /**
     * @var DateTimeImmutable
     */
    private $date;

    /**
     * @var string
     */
    private $geo;

    /**
     * @var int
     */
    private $zone;

    /**
     * @var int
     */
    private $impressions;

    /**
     * @var float
     */
    private $revenue;

    /**
     * Set date.
     *
     * @param DateTimeImmutable $date
     *
     * @return Report
     */
    public function setDate(DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return DateTimeImmutable
     */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * Set geo.
     *
     * @param string $geo
     *
     * @return Report
     */
    public function setGeo(string $geo): self
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo.
     *
     * @return string
     */
    public function getGeo(): string
    {
        return $this->geo;
    }

    /**
     * Set zone.
     *
     * @param int $zone
     *
     * @return Report
     */
    public function setZone(int $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone.
     *
     * @return int
     */
    public function getZone(): int
    {
        return $this->zone;
    }

    /**
     * Set impressions.
     *
     * @param int $impressions
     *
     * @return Report
     */
    public function setImpressions(int $impressions): self
    {
        $this->impressions = $impressions;

        return $this;
    }

    /**
     * Get impressions.
     *
     * @return int
     */
    public function getImpressions(): int
    {
        return $this->impressions;
    }

    /**
     * Set revenue.
     *
     * @param float $revenue
     *
     * @return Report
     */
    public function setRevenue(float $revenue): Report
    {
        $this->revenue = $revenue;

        return $this;
    }

    /**
     * Get revenue.
     *
     * @return float
     */
    public function getRevenue(): float
    {
        return $this->revenue;
    }
}
