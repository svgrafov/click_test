<?php
declare(strict_types=1);

namespace AppBundle\Command;

use AppBundle\Service\ImportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ReadCsvReports extends Command
{
    protected static $defaultName = 'app:read-csv';

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var ImportService
     */
    private $service;

    /**
     * ReadCsvReports constructor.
     * @param ImportService $service
     * @param Finder $finder
     */
    public function __construct(ImportService $service, Finder $finder)
    {
        $this->finder = $finder;
        $this->service = $service;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Reads csv reports and imports data to database.')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to the reports directory.')

        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Doctrine\DBAL\DBALException
     * @throws \League\Csv\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Reading reports',
        ]);
        $path = $input->getArgument('path');
        $this->finder->files()->name('*.csv')->in($path);

        foreach ($this->finder as $file) {
            if ($file->isReadable()){
                if (!$this->service->import($file->openFile())){
                    foreach ($this->service->getErrors() as $error) {
                        $output->writeln($error);
                    }
                }
            }
        }

        $output->writeln([
            'Ok',
        ]);
    }
}