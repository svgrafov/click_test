<?php
declare(strict_types=1);

namespace AppBundle\Service;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ReportValidator
{
    /** @var ValidatorInterface */
    private $validator;

    /**
     * ReportValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($record): ConstraintViolationListInterface
    {
        $groups = new Assert\GroupSequence(['Default', 'custom']);

        $constraint = new Assert\Collection([
            'date' => new Assert\Date(),
            'geo' => new Assert\Length(['min' => 2, 'max' => 2]),
            'zone' => new Assert\Range(['min' => 1, 'max' => 9999999]),
            'impressions' => new Assert\Type('numeric'),
            'revenue' => new Assert\Type('numeric'),
        ]);

        return $this->validator->validate($record, $constraint, $groups);
    }
}