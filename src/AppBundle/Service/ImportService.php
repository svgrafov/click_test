<?php
declare(strict_types=1);
namespace AppBundle\Service;

use AppBundle\Repository\ReportRepository;
use Doctrine\DBAL\DBALException;
use League\Csv\Exception;
use League\Csv\Reader;
use SplFileObject;

class ImportService
{
    /** @var ReportRepository */
    private $repository;
    /** @var ReportValidator */
    private $validator;

    private $errors;

    const
        HEADER = ['date','geo','zone','impressions','revenue'],
        BUFFER_SIZE = 1000;

    public function __construct(ReportRepository $repository, ReportValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * @param SplFileObject $file
     *
     * @return bool true в случае успеха, false в случае ошибок
     *
     * @throws DBALException
     * @throws Exception
     */
    public function import(SplFileObject $file): bool
    {
        $this->errors = [];
        $buffer = [];
        $csv = Reader::createFromFileObject($file);
        $csv->setHeaderOffset(0);
        if (!$file->current() || self::HEADER !== $csv->getHeader()){
            $this->errors[] = 'Wrong format in file ' . $file->getPathname();
            return false;
        }

        foreach ($csv->getRecords() as $offset => $record) {
            $violations = $this->validator->validate($record);

            if (count($violations)){
                $this->errors[] = 'Wrong format in file ' . $file->getPathname() . ", line " . $offset;
                continue;
            } else {
                $key = $record['date'] . $record['geo'] . $record['zone'];
                if (!isset($buffer[$key])){
                    $buffer[$key] = $record;
                } else {
                    $buffer[$key]['impressions'] = $buffer[$key]['impressions'] + $record['impressions'];
                    $buffer[$key]['revenue'] = $buffer[$key]['revenue'] + $record['revenue'];
                }
            }

            if (count($buffer) >= self::BUFFER_SIZE) {
                $this->repository->import(array_values($buffer));
                $buffer = [];
            }
        }
        $this->repository->import(array_values($buffer));
        return $this->errors ? false : true;
    }

    /**
     * @return array|string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}